
CREATE DATABASE Pokedex;

CREATE TABLE pokemon(
   Nat   INTEGER  NOT NULL PRIMARY KEY 
  ,Name  VARCHAR(16) NOT NULL
  ,Type  VARCHAR(12)
  ,HP    INTEGER 
  ,Spd   INTEGER 
  ,BST   INTEGER 
);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (442,'Spiritomb','GHOSTDARK',50,35,485);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (443,'Gible','DRAGONGROUND',58,42,300);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (444,'Gabite','DRAGONGROUND',68,82,410);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (445,'Garchomp','DRAGONGROUND',108,102,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (446,'Munchlax','NORMAL',135,5,390);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (447,'Riolu','FIGHT',40,60,285);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (448,'Lucario','FIGHTSTEEL',70,90,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (449,'Hippopotas','GROUND',68,32,330);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (450,'Hippowdon','GROUND',108,47,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (451,'Skorupi','POISONBUG',40,65,330);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (452,'Drapion','POISONDARK',70,95,500);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (453,'Croagunk','POISONFIGHT',48,50,300);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (454,'Toxicroak','POISONFIGHT',83,85,490);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (455,'Carnivine','GRASS',74,46,454);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (456,'Finneon','WATER',49,66,330);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (457,'Lumineon','WATER',69,91,460);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (458,'Mantyke','WATERFLYING',45,50,345);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (459,'Snover','GRASSICE',60,40,334);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (460,'Abomasnow','GRASSICE',90,60,494);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (460,'Mega Abomasnow','GRASSICE',90,30,594);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (461,'Weavile','DARKICE',70,125,510);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (462,'Magnezone','ELECTRSTEEL',70,60,535);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (463,'Lickilicky','NORMAL',110,50,515);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (464,'Rhyperior','GROUNDROCK',115,40,535);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (465,'Tangrowth','GRASS',100,50,535);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (466,'Electivire','ELECTR',75,95,540);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (467,'Magmortar','FIRE',75,83,540);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (468,'Togekiss','FAIRYFLYING',85,80,545);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (469,'Yanmega','BUGFLYING',86,95,515);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (470,'Leafeon','GRASS',65,95,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (471,'Glaceon','ICE',65,65,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (472,'Gliscor','GROUNDFLYING',75,95,510);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (473,'Mamoswine','ICEGROUND',110,80,530);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (474,'Porygon-Z','NORMAL',85,90,535);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (475,'Gallade','PSYCHCFIGHT',68,80,518);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (476,'Probopass','ROCKSTEEL',60,40,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (477,'Dusknoir','GHOST',45,45,525);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (478,'Froslass','ICEGHOST',70,110,480);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (479,'Rotom','ELECTRGHOST',50,91,440);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (480,'Uxie','PSYCHC',75,95,580);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (481,'Mesprit','PSYCHC',80,80,580);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (482,'Azelf','PSYCHC',75,115,580);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (483,'Dialga','STEELDRAGON',100,90,680);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (484,'Palkia','WATERDRAGON',90,100,680);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (485,'Heatran','FIRESTEEL',91,77,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (486,'Regigigas','NORMAL',110,100,670);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (487,'Giratina','GHOSTDRAGON',150,90,680);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (488,'Cresselia','PSYCHC',120,85,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (489,'Phione','WATER',80,80,480);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (490,'Manaphy','WATER',100,100,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (491,'Darkrai','DARK',70,125,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (492,'Shaymin','GRASS',100,100,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (492,'Shaymin','GRASSFLYING',100,127,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (493,'Arceus','NORMAL',120,120,720);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (494,'Victini','PSYCHCFIRE',100,100,600);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (495,'Snivy','GRASS',45,63,308);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (496,'Servine','GRASS',60,83,413);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (497,'Serperior','GRASS',75,113,528);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (498,'Tepig','FIRE',65,45,308);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (499,'Pignite','FIREFIGHT',90,55,418);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (500,'Emboar','FIREFIGHT',110,65,528);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (501,'Oshawott','WATER',55,45,308);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (502,'Dewott','WATER',75,60,413);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (503,'Samurott','WATER',95,70,528);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (504,'Patrat','NORMAL',45,42,255);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (505,'Watchog','NORMAL',60,77,420);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (506,'Lillipup','NORMAL',45,55,275);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (507,'Herdier','NORMAL',65,60,370);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (508,'Stoutland','NORMAL',85,80,500);
INSERT INTO pokemon(Nat,Name,Type,HP,Spd,BST) VALUES (509,'Purrloin','DARK',41,66,281);





CREATE TABLE TypeT(
   Type           VARCHAR(8) NOT NULL PRIMARY KEY
  ,Strong_Against VARCHAR(35)
  ,Weak_Against   VARCHAR(51) NOT NULL
  ,Resistant_To   VARCHAR(76) NOT NULL
  ,Vulnerable_To  VARCHAR(37) NOT NULL
  ,FIELD6         VARCHAR(30)
  ,FIELD7         VARCHAR(30)
  ,FIELD8         VARCHAR(30)
);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Normal',NULL,'Rock, Ghost, Steel','Ghost','Fighting',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Fighting','Normal, Rock, Steel, Ice, Dark','Flying, Poison, Psychic, Bug, Ghost, Fairy','Rock, Bug, Dark','Flying, Psychic, Fairy',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Flying','Fighting, Bug, Grass','Rock, Steel, Electric','Fighting, Ground, Bug, Grass','Rock, Electric, Ice',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Poison','Grass, Fairy','Poison, Ground, Rock, Ghost, Steel','Fighting, Poison, Grass, Fairy','Ground, Psychic',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Ground','Poison, Rock, Steel, Fire, Electric','Flying, Bug, Grass','Poison, Rock, Electric','Water, Grass, Ice',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Rock','Flying, Bug, Fire, Ice','Fighting, Ground, Steel','Normal, Flying, Poison, Fire','Fighting, Ground, Steel, Water, Grass',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Bug','Grass, Psychic, Dark','Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy','Fighting, Ground, Grass','Flying, Rock, Fire',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Ghost','Ghost, Psychic','Normal, Dark','Normal, Fighting, Poison, Bug','Ghost, Dark',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Steel','Rock, Ice, Fairy','Steel, Fire, Water, Electric','Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy','Fighting, Ground, Fire',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Fire','Bug, Steel, Grass, Ice','Rock, Fire, Water, Dragon','Bug, Steel, Fire, Grass, Ice','Ground, Rock, Water',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Water','Ground, Rock, Fire','Water, Grass, Dragon','Steel, Fire, Water, Ice','Grass, Electric',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Grass','Ground, Rock, Water','Flying, Poison, Bug, Steel, Fire, Grass, Dragon','Ground, Water, Grass, Electric','Flying, Poison, Bug, Fire, Ice',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Electric','Flying, Water','Ground, Grass, Electric, Dragon','Flying, Steel, Electric','Ground',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Psychic','Fighting, Poison','Steel, Psychic, Dark','Fighting, Psychic','Bug, Ghost, Dark',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Ice','Flying, Ground, Grass, Dragon','Steel, Fire, Water, Ice','Ice','Fighting, Rock, Steel, Fire',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Dragon','Dragon','Steel, Fairy','Fire, Water, Grass, Electric','Ice, Dragon, Fairy',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Fairy','Fighting, Dragon, Dark','Poison, Steel, Fire','Fighting, Bug, Dragon, Dark','Poison, Steel',NULL,NULL,NULL);
INSERT INTO TypeT(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To,FIELD6,FIELD7,FIELD8) VALUES ('Dark','Ghost, Psychic','Fighting, Dark, Fairy','Ghost, Psychic, Dark','Fighting, Bug, Fairy',NULL,NULL,NULL);




CREATE TABLE Attack(
   Name VARCHAR(16) NOT NULL PRIMARY KEY
  ,Atk  INTEGER 
  ,Def  INTEGER 
  ,Sat  INTEGER 
  ,SDf  INTEGER 
);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Spiritomb',92,108,92,108);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Gible',70,45,40,45);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Gabite',90,65,50,55);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Garchomp',130,95,80,85);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mega Garchomp',170,115,120,95);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Munchlax',85,40,40,85);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Riolu',70,40,35,40);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Lucario',110,70,115,70);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mega Lucario',145,88,140,70);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Hippopotas',72,78,38,42);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Hippowdon',112,118,68,72);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Skorupi',50,90,30,55);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Drapion',90,110,60,75);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Croagunk',61,40,61,40);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Toxicroak',106,65,86,65);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Carnivine',100,72,90,72);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Finneon',49,56,49,61);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Lumineon',69,76,69,86);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mantyke',20,50,60,120);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Snover',62,50,62,60);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Abomasnow',92,75,92,85);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mega Abomasnow',132,105,132,105);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Weavile',120,65,45,85);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Magnezone',70,115,130,90);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Lickilicky',85,95,80,95);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Rhyperior',140,130,55,55);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Tangrowth',100,125,110,50);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Electivire',123,67,95,85);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Magmortar',95,67,125,95);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Togekiss',50,95,120,115);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Yanmega',76,86,116,56);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Leafeon',110,130,60,65);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Glaceon',60,110,130,95);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Gliscor',95,125,45,75);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mamoswine',130,80,70,60);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Porygon-Z',80,70,135,75);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Gallade',125,65,65,115);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mega Gallade',165,95,65,115);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Probopass',55,145,75,150);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Dusknoir',100,135,65,135);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Froslass',80,70,80,70);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Rotom',50,77,95,77);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Rotom',65,107,105,107);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Uxie',75,130,75,130);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Mesprit',105,105,105,105);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Azelf',125,70,125,70);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Dialga',120,120,150,100);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Palkia',120,100,150,120);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Heatran',90,106,130,106);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Regigigas',160,110,80,110);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Giratina',100,120,100,120);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Cresselia',70,120,75,130);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Phione',80,80,80,80);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Manaphy',100,100,100,100);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Darkrai',90,90,135,90);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Shaymin',100,100,100,100);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Arceus',120,120,120,120);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Victini',100,100,100,100);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Snivy',45,55,45,55);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Servine',60,75,60,75);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Serperior',75,95,75,95);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Tepig',63,45,45,45);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Pignite',93,55,70,55);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Emboar',123,65,100,65);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Oshawott',55,45,63,45);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Dewott',75,60,83,60);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Samurott',100,85,108,70);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Patrat',55,39,35,39);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Watchog',85,69,60,69);
INSERT INTO Attack(Name,Atk,Def,Sat,SDf) VALUES ('Lillipup',60,45,25,45);


delimiter $$
create procedure getTypes()
begin
Select Type from TypeT;
end $$