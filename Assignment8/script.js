$("#menu").click(function () {
    $(".point").css("display", "inline-block");
    $(".point").animate({
        left: "0px",
        top: "0px"
    }, 1000);
    $(".menu").css("transform", "rotate(-25deg)");
    $("li").css({ "display": "inline-block" });

    for (var i = 1; i <= 5; i++) {
        $("#" + i).animate({
            left: (i * 105) + "px"
        }, 1500)
    }
});

var topValues = [0, 50, 92, 132, 175, 215];
var leftValues = [0, 20, 110, 200, 294, 384];

for (var i = 1; i <= 5; i++) {
    $("#" + i).click(function () {
        $(".point").animate({
            left: leftValues[this.id] + "px",
            top: topValues[this.id] + "px"
        }, 1000, function () {
            window.location.replace("https://www.youtube.com/");
        })
    })
}