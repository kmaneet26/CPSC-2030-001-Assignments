<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);

include 'templates/header.php';
?>

		<h3>Know More About Us..</h3>

        <div class="homecol1">
            <h2> Raise Funds for our Recent Program</h2>
			<p id="matter1">
				Help support the workers who were affected by Hydrogen explosion in Fukushima Dai-ichi nuclear power plant
				in Japan which injured a total of six.
            </p>
			 <img src="Images/japanexplosion.jpg" alt="Riley Hale" style="width:500px;height:400px;"> <p id="matter1">
			 <div class="donation">
		   <!-- <p>  $20 <button type='submit' onClick="location.href='About.html'"> Donate </button> </p> -->
			<!-- <p> $50 <button type='submit'> Donate </button> </p>
			<p> $100 <button type='submit'> Donate </button> </p>
			 <p>$500 <button type='submit'> Donate </button> </p> -->
       <?php
 			echo $twig-> render('donation.html',array (
 			'money' => array('$20','$30','$40','$50')
 			));
 	 ?>
			 <form>
			 <label>Add your own amount:</label><br> <textarea name="donateInput" id="donateInput"></textarea> <br>
			 <button class="donateCustom" type='button'> Donate </button>
			 <p class="message" style="margin-bottom:20px;color:red;"><?php if(isset($_GET['message'])){ echo $_GET['message']; }?> </p>
			  </form>
			  </div>
			</div>
        <div class="homecol2">

            <h2> Contact Us </h2>
            <p id="matter1">
                Provide Us Feedback<br />
                Email: HelpInNeed@gmail.com<br />
                Contact: 613-555-0199

         </p>
        </div>

        <?php
       echo $twig-> render('footer.html');
    ?>
