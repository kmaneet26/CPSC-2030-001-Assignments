<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);

include 'templates/header.php';
?>
<style>

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background:  #003	;
	
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
 background-color:#0069A8;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
 background-color:#000;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 16px;
}
.form .message a {
  color: #0069A8;
  text-decoration: none;
}
.form .register-form {
  display: none;
}

</style>
		

        <div class="homecol1">
            <div class="login-page">

  <div class="form">
  
<p class="message" style="margin-bottom:20px"><font color="red"><?php if(!isset($_SESSION["errorMessage"])){}else if($_SESSION["errorMessage"]=="success"){ echo 'Account Created Successfully';
$_SESSION["errorMessage"]=NULL;
}
else
{
	echo $_SESSION["errorMessage"]; 
$_SESSION["errorMessage"]=NULL;}?></font></p>	
    <form id="register" action="loginResponse.php" method="post" class="register-form">
	<h3>Register</h3>
     	<input type="text" placeholder="Full Name (e.g. John)" name="name"/>
     	<input type="email" placeholder="Email (e.g. example@gmail.com)" name="email"/>
      	<input type="password" placeholder="Password"name="password"/>
		<input type="text" placeholder="Confirm Password" name="cpassword"/>
	<input type="hidden" name="form_register" value="1" />	
		
      <button>create</button>
      <p class="message">Already registered? <a href="#login" onclick="change('login')">Sign In</a></p>
    </form>
    <form id="login" action="loginResponse.php" method="post" class="login-form">
	<h3>Login</h3>
      <input type="email" placeholder="Email (e.g. example@gmail.com)" name="email_login"/>
      <input type="password" placeholder="Password" name="password_login"/>
<input type="hidden" name="form_register" value="0" />
      <button>login</button>
      <p class="message">Not registered? <a href="#register" onclick="change('register')">Create an account</a></p>
    </form>
  </div>
</div>
	<script>
		function change(val) {
    if (val== 'login') {
       document.getElementById("register").style.display = "none";
		document.getElementById("login").style.display = "block";
    }    
  if (val== 'register') {	  
       document.getElementById("register").style.display = "block";
		document.getElementById("login").style.display = "none";
    }    
	   
}

</script>
        </div>

        <div class="homecol2">

            <h2> Contact Us </h2>
            <p id="matter1">
                Provide Us Feedback<br />
                Email: HelpInNeed@gmail.com<br />
                Contact: 613-555-0199

         </p>
        </div>






				<?php
			 echo $twig-> render('footer.html');
		?>
