function character(name,side,unit,rank,role,desc,img){
  return { name:name, side:side, unit:unit, rank:rank, role:role, description:desc, image:img }
}
var characterArray = [
  character("Claude Wallace","Edinburgh Army","Ranger Corps, Squad E","First Lieutenant","Tank Commander","Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.","http://valkyria.sega.com/img/character/chara-ss01.jpg"),
  character("Riley Miller","Edinburgh Army","Federate Joint Ops","Second Lieutenant","Artillery Advisor","Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.","http://valkyria.sega.com/img/character/chara-ss02.jpg"),
  character("Raz","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Fireteam Leader","Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.","http://valkyria.sega.com/img/character/chara-ss03.jpg"),
  character("Kai Schulen","Edinburgh Army","Ranger Corps, Squad E","Sergeant Major","Fireteam Leader","Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai.' Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.","http://valkyria.sega.com/img/character/chara-ss04.jpg"),
  character("Minerva Victor","Edinburgh Army","Ranger Corps, Squad F","First Lieutenant","Senior Commander","Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.","http://valkyria.sega.com/img/character/chara-ss11.jpg"),
  character("Karen Stuart","Edinburgh Army","Squad E","Corporal","Combat EMT","Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.","http://valkyria.sega.com/img/character/chara-ss12.jpg"),
  character("Ragnarok","Edinburgh Army","Squad E","K-9 Unit","Mascot","Once a stray, this good good boy is lovingly referred to as 'Rags.'As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.","http://valkyria.sega.com/img/character/chara-ss13.jpg"),
  character("Miles Arbeck","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Tank Operator","Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.","http://valkyria.sega.com/img/character/chara-ss15.jpg"),
  character("Dan Bentley","Edinburgh Army","Ranger Corps, Squad E","Private First Class","APC Operator","Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.","http://valkyria.sega.com/img/character/chara-ss16.jpg"),
  character("Ronald Albee","Edinburgh Army","Ranger Corps, Squad E","Second Lieutenant","Tank Operator","Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.","http://valkyria.sega.com/img/character/chara-ss17.jpg")
];

var squad = [];

var list = document.getElementById('list');
var roaster = document.getElementById('roaster');
var profile = document.getElementById('profile');
var image = document.getElementById('image');

function createList(){
    let listHTML = "";
    for(let i=0;i<characterArray.length;i++){
      listHTML += "<div onclick=\"createProfile('"+characterArray[i].name+"')\">";
      listHTML += characterArray[i].name;
      listHTML += "</div>";
    }
    list.innerHTML=listHTML;
}

function createProfile(name){
  for(let i=0;i<characterArray.length;i++){
    if(name==characterArray[i].name){
      var hasSquad=false;
      for(var j=0;j<squad.length;j++){
        if(characterArray[i].name==squad[j].name){
          hasSquad=true;
          squad.splice(j,1);
          displaySquad();
        }
      }
      if(!hasSquad){
        squad.push(character(characterArray[i].name,characterArray[i].side,characterArray[i].unit,characterArray[i].rank,characterArray[i].role,characterArray[i].description,characterArray[i].image))
        displaySquad();
      }
      profile.innerHTML = "<ul>"
                          +"<li>Name: "+characterArray[i].name+"</li>"
                          +"<li>Side: "+characterArray[i].side+"</li>"
                          +"<li>Unit: "+characterArray[i].unit+"</li>"
                          +"<li>Rank: "+characterArray[i].rank+"</li>"
                          +"<li>Role: "+characterArray[i].role+"</li>"
                          +"<li>Description: "+characterArray[i].description+"</li>"
                          +"</ul>";
      image.innerHTML = "<img src='"+characterArray[i].image+"' >"
    }
  }
}

function displaySquad(){
  var roasterHTML ="<ul>";
  for(var i=0;i<squad.length;i++){
    roasterHTML = roasterHTML+ "<li>"+squad[i].name+"</li>";
  }
  var roasterHTML = roasterHTML+"</ul>";
  roaster.innerHTML = roasterHTML;
}
